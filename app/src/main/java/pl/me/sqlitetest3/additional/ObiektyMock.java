package pl.me.sqlitetest3.additional;


import java.util.LinkedList;
import java.util.List;

/**
 * Created by Jarek on 5/16/2016.
 */
public class ObiektyMock {
    private List<Obiekt> Obiekty = new LinkedList<>();
    public ObiektyMock(){
        Obiekty.add(new Obiekt("Dom Towarowy",10,20));
        Obiekty.add(new Obiekt("Kościół pw. Grobu Bożego",0,0));
        Obiekty.add(new Obiekt("Galeria Sztuki \"U Jaksy\"",5,15));
        Obiekty.add(new Obiekt("Galeria Krakowska",15,20));
        Obiekty.add(new Obiekt("Rynek Główny",0,0));
        Obiekty.add(new Obiekt("Kino ARS",0,10));
        Obiekty.add(new Obiekt("Multikino",5,10));
        Obiekty.add(new Obiekt("Kino Kijów.Centrum",5,0));
        Obiekty.add(new Obiekt("Kino Sfinks",5,5));
        Obiekty.add(new Obiekt("Park Lotników",0,0));
        Obiekty.add(new Obiekt("Park Jordana",0,0));
        Obiekty.add(new Obiekt("Błonia",0,0));
        Obiekty.add(new Obiekt("Stadion Wisły",5,10));
        Obiekty.add(new Obiekt("Stadion Cracovii",5,5));
        Obiekty.add(new Obiekt("Muzeum Narodowe",0,15));
        Obiekty.add(new Obiekt("Wawel",5,0));
        Obiekty.add(new Obiekt("Ogród Botaniczny",0,20));
        Obiekty.add(new Obiekt("Ogród Zoologiczny",0,10));
        Obiekty.add(new Obiekt("Stadion AWF",0,0));
        Obiekty.add(new Obiekt("Tauron Arena",10,5));
        Obiekty.add(new Obiekt("Muzeum Lotnictwa",5,5));
        Obiekty.add(new Obiekt("Nowohuckie Centrum Kultury",0,10));
        Obiekty.add(new Obiekt("Łąki Nowohuckie",0,0));
        Obiekty.add(new Obiekt("Zalew Nowohucki",0,0));
        Obiekty.add(new Obiekt("Galeria Kazimierz",0,15));
    }

    public List<Obiekt> getObiekty(){
        return Obiekty;
    }
    public Obiekt getObiekt(String obj){
        for(Obiekt obiekt : Obiekty){
            if(obiekt.nazwa.equals(obj)){
                return obiekt;
            }
        }
        return null;
    }

    public class Obiekt{
        Obiekt(String nz, int pr, int cn){
            nazwa = nz;
            promocja = pr;
            cena = cn;
        }
        private String nazwa;
        private int promocja;
        private int cena;

        public int getPromocja() {
            return promocja;
        }

        public int getCena() {
            return cena;
        }
    }
}
