package pl.me.sqlitetest3.additional;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Piotr1 on 2016-04-10.
 */
public class Data {
    private int rok;
    private int miesiac;
    private int dzien;
    private int godzina;
    private int minuta;

    public Data(int rok, int miesiac, int dzien, int godzina, int minuta) {
        this.rok = rok;
        this.miesiac = miesiac;
        this.dzien = dzien;
        this.godzina = godzina;
        this.minuta = minuta;
    }

    public int getRok() {
        return rok;
    }

    public int getMiesiac() {
        return miesiac;
    }

    public int getDzien() {
        return dzien;
    }

    public int getGodzina() {
        return godzina;
    }

    public int getMinuta() {
        return minuta;
    }

    public static long convertDataToMilis(String dataText) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMAN);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
//        String text = this.rok + "-" + String.format("%02d",this.miesiac) + "-" + String.format("%02d",this.dzien) + " " + String.format("%02d",this.godzina) + ":" + String.format("%02d",this.minuta) + ":" + String.format("%02d",this.sekunda);

        Date date = null;
        try {
            date = format.parse(dataText);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date.getTime();
    }

    public static String convertMilisToData(long milis) {
        Date date = new Date(milis);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.GERMAN);

        return formatter.format(date);
    }



    @Override
    public String toString() {
        return "Data{" +
                "rok=" + rok +
                ", miesiac=" + miesiac +
                ", dzien=" + dzien +
                ", godzina=" + godzina +
                ", minuta=" + minuta +
                '}';
    }
}
