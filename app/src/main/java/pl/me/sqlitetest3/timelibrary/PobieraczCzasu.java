package pl.me.sqlitetest3.timelibrary;

import org.json.JSONArray;
import org.json.JSONObject;

import pl.me.sqlitetest3.additional.Point;

public class PobieraczCzasu {
    private static final String API_KEY = "AIzaSyAP7dMX1At-NL62WICuMgb1s-kAX6BLrH4";
    public PobieraczCzasu() {   }

    String modeOfTransport;

    public String getRealTime(Point homePlace, Point chosenPlace, String meanOfTransport, boolean optimal) {
        switch (meanOfTransport){
            case "Pieszo": modeOfTransport = "walking"; break;
            case "Rower": modeOfTransport = "bicycling"; break;
            case "Samochód": modeOfTransport = "driving"; break;
            case "Transport publiczny": modeOfTransport = "transit"; break;
        }

        String url_request = "";

        if(optimal == true){
            url_request += "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" +
                    homePlace.getLatitude() +"," +
                    homePlace.getLongitude() + "&destinations=" + chosenPlace.getLatitude()+ ", " + chosenPlace.getLongitude()+
                    "&mode=" + modeOfTransport + "&traffic_model=optimistic&departure_time=now&language=en-EN&key="
                    + API_KEY;
        } else {
            url_request += "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" +
                    homePlace.getLatitude() +"," +
                    homePlace.getLongitude() + "&destinations=" + chosenPlace.getLatitude()+ ", " + chosenPlace.getLongitude()+
                    "&mode=" + modeOfTransport + "&departure_time=now&language=en-EN&key="
                    + API_KEY;
        }

        GoogleMatrixRequest request = new GoogleMatrixRequest();
        try {
            String res = request.execute(url_request).get();
            JSONObject root = new JSONObject(res);

            String response = (String) root.getJSONArray("rows").getJSONObject(0).getJSONArray("elements")
                    .getJSONObject(0).getJSONObject("duration").get("text");
            return response;
        } catch (Exception e){
            System.err.println(e.getMessage());
        }
        return "---";
    }
}
