package pl.me.sqlitetest3.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.softeq.android.prepopdb.R;

import java.util.Objects;

import pl.me.sqlitetest3.datamgmt.ExternalDbOpenHelper;

public class OptionsActivity extends Activity {
    private SQLiteDatabase database;
    private EditText eText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(this, "database_test.db");
        database = dbOpenHelper.openDataBase();
        createTableIfNotExists();

        eText = (EditText)findViewById(R.id.editText);
        eText.setText(getCurrentKeyWord());

    }

    private void createTableIfNotExists(){

         final String createQuery = "CREATE TABLE IF NOT EXISTS options ("+
                 "option_name varchar(50) PRIMARY KEY,"+
                 "option_value varchar(255)"+
                ");";
         database.execSQL(createQuery);
    }

    private String getCurrentKeyWord(){
        Cursor cursor = database.query("options", new String[]{"option_name", "option_value"},null,null,null,null,null);

        if (cursor.moveToFirst() && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String optionName = cursor.getString(0);
                String value = cursor.getString(1);
                System.out.println("optionName: "+optionName+", optionValue: "+value);
                if(optionName.contains("keyWord")) {
                    System.out.println("Zwracam znalezioną wartość!");
                    return value;
                }
            } while (cursor.moveToNext());
        }
        return "";
    }

    public void setKeyWord(View v){
        String keyWord = eText.getText().toString();
        ContentValues contentValues = new ContentValues();
        if (keyWord.equals("")) keyWord = "sport";

        contentValues.clear();
        contentValues.put("option_name","keyWord");
        contentValues.put("option_value", keyWord);

        if (getCurrentKeyWord().equals("")) {
            System.out.println("Dodaję");
            database.insert("options", null, contentValues);
        }else {
            System.out.println("Aktualizuję");
            final String updateString = "UPDATE options SET option_value='"+keyWord+"' WHERE option_name='keyWord'";
            database.execSQL(updateString);
        }

        Context context = getApplicationContext();
        CharSequence text = "Zapisano zmiany";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}
