package pl.me.sqlitetest3.activities;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

import com.softeq.android.prepopdb.R;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import pl.me.sqlitetest3.additional.ObiektyMock;
import pl.me.sqlitetest3.calendarlibrary.CalendarOperations;
import pl.me.sqlitetest3.datamgmt.ActivityPlace;
import pl.me.sqlitetest3.datamgmt.ExternalDbOpenHelper;
import pl.me.sqlitetest3.additional.Point;


public class SummaryActivity extends Activity {
    private SQLiteDatabase database;
    TextView choiceText, pointsText;
    Point homePlace, chosenPoint;
    String meanOfTransport;

    TextView bestPlace1, bestPlace2, bestPlace3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        //Our key helper
        ExternalDbOpenHelper dbOpenHelper = new ExternalDbOpenHelper(this, "database_test.db");
        database = dbOpenHelper.openDataBase();
        //That’s it, the database is open!

        choiceText = (TextView) findViewById(R.id.choice_value);
        pointsText = (TextView) findViewById(R.id.points);

        //chosenPlace = (ActivityPlace) getIntent().getSerializableExtra("ChosenPlace");
        int chosenPlaceIndex = Integer.parseInt(getIntent().getStringExtra("chosenPlaceIndex"));
        ActivityPlace chosenPlace = MainActivity.places.get(chosenPlaceIndex);
        chosenPoint = new Point(chosenPlace.getLongitude(), chosenPlace.getLatitude());

        homePlace = (Point) getIntent().getSerializableExtra("HomePlace");
        choiceText.setText(chosenPlace.getName() + ", cel: " + chosenPlace.getActivityType());
        meanOfTransport = (String) getIntent().getSerializableExtra("MeanOfTransport");

        ObiektyMock additionalData = new ObiektyMock();
        CalendarOperations calendar = new CalendarOperations(this);
        String keyword = getCurrentKeyWord();
        for (int i=0; i<MainActivity.places.size(); i++) {
            ActivityPlace ap = MainActivity.places.get(i);
            String optimalRideTime2 = "0";
            try {
                optimalRideTime2 = String.valueOf(Integer.parseInt(ap
                        .getRealRideTime(homePlace, meanOfTransport, true)) -10);
            } catch (NumberFormatException e) {    }
            ap.setScore(ap.computeScore(
                    ap.getSmog(),
                    ap.getWeather(),
                    ap.getRealRideTime(homePlace, meanOfTransport, false),
                    optimalRideTime2,
                    ap.getChoicesCount(database),
                    additionalData.getObiekt(ap.getName()),
                    chosenPlace.getActivityType(),
                    ap.getMeanAcitivtyTime(calendar, keyword),
                    calendar.zwrocCzasWolnyOdDaty(24)));
        }
        pointsText.setText(String.valueOf(chosenPlace.getScore()));
        List<ActivityPlace> localPlacesList = new LinkedList<>(MainActivity.places);
        Collections.sort(localPlacesList);
        bestPlace1 = (TextView) findViewById(R.id.recommendation1);
        bestPlace2 = (TextView) findViewById(R.id.recommendation2);
        bestPlace3 = (TextView) findViewById(R.id.recommendation3);

        if (chosenPlace == localPlacesList.get(0))
            bestPlace1.setTypeface(null, Typeface.BOLD);
        else if (chosenPlace == localPlacesList.get(1))
            bestPlace2.setTypeface(null, Typeface.BOLD);
        else if (chosenPlace == localPlacesList.get(2))
            bestPlace3.setTypeface(null, Typeface.BOLD);
        bestPlace1.setText(localPlacesList.get(0).getName()+" ("+localPlacesList.get(0).getStreet()+
                ") "+localPlacesList.get(0).getActivityType()+" "+localPlacesList.get(0).getScore());
        bestPlace2.setText(localPlacesList.get(1).getName()+" ("+localPlacesList.get(1).getStreet()+
                ") "+localPlacesList.get(1).getActivityType()+" "+localPlacesList.get(1).getScore());
        bestPlace3.setText(localPlacesList.get(2).getName()+" ("+localPlacesList.get(2).getStreet()+
                ") "+localPlacesList.get(2).getActivityType()+" "+localPlacesList.get(2).getScore());
    }

    private String getCurrentKeyWord(){
        Cursor cursor = database.query("options", new String[]{"option_name", "option_value"},null,null,null,null,null);

        if (cursor.moveToFirst() && cursor.getCount() > 0) {
            cursor.moveToFirst();
            do {
                String optionName = cursor.getString(0);
                String value = cursor.getString(1);
                System.out.println("optionName: "+optionName+", optionValue: "+value);
                if(optionName.contains("keyWord")) {
                    System.out.println("Zwracam znalezioną wartość!");
                    return value;
                }
            } while (cursor.moveToNext());
        }
        return "";
    }
}
